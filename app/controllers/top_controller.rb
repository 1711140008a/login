class TopController < ApplicationController
    def main
        if session[:login_uid]
            render 'main'
        else
            render 'login'
        end
    end
    def login
        pp params[:uid]
        pp params[:pass]
        if User.authenticate(params[:uid],params[:pass])
            session[:login_uid] = params[:uid]
            redirect_to top_main_path
        else
            render 'error'
        end
    end
    def logout
        session[:login_uid] = nil
        redirect_to top_main_path
    end
end

